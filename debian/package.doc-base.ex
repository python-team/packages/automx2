Document: automx2
Title: Debian automx2 Manual
Author: <insert document author here>
Abstract: This manual describes what automx2 is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/automx2/automx2.sgml.gz

Format: postscript
Files: /usr/share/doc/automx2/automx2.ps.gz

Format: text
Files: /usr/share/doc/automx2/automx2.text.gz

Format: HTML
Index: /usr/share/doc/automx2/html/index.html
Files: /usr/share/doc/automx2/html/*.html
