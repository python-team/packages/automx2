# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/automx2/issues
# Bug-Submit: https://github.com/<user>/automx2/issues/new
# Changelog: https://github.com/<user>/automx2/blob/master/CHANGES
# Documentation: https://github.com/<user>/automx2/wiki
# Repository-Browse: https://github.com/<user>/automx2
# Repository: https://github.com/<user>/automx2.git
